from django.contrib import messages
from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.decorators import login_required

from festhub.utils import check_profile_completed
from .models import Committee
from events.models import Event
from django.urls import reverse
from profiles.models import Profile

@login_required(login_url='accounts:login')
def committees(request):
    committees=Committee.objects.all()
    context = {
        'committees': committees,
    }
    return render(request, 'committees/committees.html',context)

@login_required(login_url='accounts:login')
def committee(request, uuid):
    if not check_profile_completed(request):
        # If profile is not complete, redirect to complete profile page
        return redirect('accounts:completeProfile', user_id=request.user.id)
    committees=get_object_or_404(Committee, uuid=uuid)
    events=Event.objects.filter(committees=committees)
    context = {
        'committees': committees,
        'events':events,
    }
    return render(request, 'committees/committee.html',context)