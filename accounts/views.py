from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, authenticate,logout
from django.contrib import messages
from .forms import CustomUserCreationForm, ProfileForm
from profiles.models import Profile
from django.contrib.auth.models import User 

def signup_view(request):
    if request.method == 'POST':
        user_form = CustomUserCreationForm(request.POST)
        if user_form.is_valid():
            username = user_form.cleaned_data.get('username')
            if CustomUserCreationForm.Meta.model.objects.filter(username=username).exists():
                messages.warning(request, 'Username already exists. Please choose a different username.')
                return redirect('accounts:signup')

            user = user_form.save()
            login(request, user) 
            messages.success(request, 'Account created successfully.')
            return redirect('accounts:completeProfile',  user_id=user.id)
        else:
            for field, errors in user_form.errors.items():
                for error in errors:
                    messages.error(request, f"{field}: {error}")
    else:
        user_form = CustomUserCreationForm()
    return render(request, 'accounts/signup.html', {'user_form': user_form})
from django.contrib import messages

def completeProfile_view(request, user_id):
    user = get_object_or_404(User, pk=user_id)
    if request.method == 'POST':
        profile_form = ProfileForm(request.POST, request.FILES)
        if profile_form.is_valid():
            if not Profile.objects.filter(user=user).exists():
                profile = profile_form.save(commit=False)
                profile.user = user
            else:
                profile = Profile.objects.get(user=user)

            profile.contact_number = profile_form.cleaned_data['contact_number']
            profile.address = profile_form.cleaned_data['address']
            profile.birth_date = profile_form.cleaned_data['birth_date']
            profile.committees.set(profile_form.cleaned_data['committees'])

            # Set optional fields
            profile.resume = request.FILES.get('resume')
            profile.image = request.FILES.get('image')
            profile.department_year = profile_form.cleaned_data['department_year']
            profile.blood_group = profile_form.cleaned_data['blood_group']

            # Check if all required fields are filled except committees
            required_fields_filled = all([
                profile.contact_number,
                profile.address,
                profile.birth_date,
            ])

            if required_fields_filled:
                profile.is_profile_complete = True
                profile.save()
                return redirect('dashboard')
            else:
                profile.is_profile_complete = False
                profile.save()
                messages.warning(request, 'Please fill all required fields to access all features.')
                return redirect('accounts:completeProfile', user_id=user_id)
        else:
            for field, errors in profile_form.errors.items():
                for error in errors:
                    messages.error(request, f"{field}: {error}")
    else:
        profile_form = ProfileForm()
    return render(request, 'accounts/completeProfile.html', {'user': user, 'profile_form': profile_form})

def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(data = request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            if 'next' in request.POST:
                return redirect(request.POST.get('next'))
            return redirect('dashboard')

    else:
        form = AuthenticationForm()
    return render(request, 'accounts/login.html', {'form':form})

def logout_view(request):
    if request.method == 'POST':
        logout(request)
        return redirect('home')
