from django.urls import path, include
from . import views

app_name = 'accounts'

urlpatterns= [
    path('signup/', views.signup_view, name='signup'),
    path('login/', views.login_view, name='login'),
    path('logout/', views.logout_view, name='logout'),
    path('completeProfile/<int:user_id>', views.completeProfile_view, name='completeProfile'),
]
