from django.shortcuts import render,redirect, get_object_or_404
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.decorators import login_required
from committees.models import Committee
from events.models import Event
from festhub.utils import check_profile_completed
from profiles.models import Profile
from events.models import UserEvent
from django.contrib.auth.models import User

def homepage(request):
    event=Event.objects.all()
    committee=Committee.objects.all()
    context = {
        'event': event,
        'committee': committee,
    }
    return render(request, 'homepage/index.html',context)

@login_required(login_url='accounts:login')
def dashboard(request):
    if not check_profile_completed(request):
        # If profile is not complete, redirect to complete profile page
        return redirect('accounts:completeProfile', user_id=request.user.id)
    events = Event.objects.all()
    context = {
        'events':events,
        'user': request.user,
    }
    return render(request, 'dashboard.html', context)

def registered_events(request):
    if not check_profile_completed(request):
        # If profile is not complete, redirect to complete profile page
        return redirect('accounts:completeProfile', user_id=request.user.id)
    events = Event.objects.filter(interests__user=request.user.id, interests__registered=True)      #The events passed here are registered events objects of events
    registered_events = UserEvent.objects.filter(user=request.user, registered=True).values_list('event__uuid', flat=True)
    pinned_events = UserEvent.objects.filter(user=request.user, pinned=True).values_list('event__uuid', flat=True)
    context = {
        'events':events,
        'user': request.user,
        'registered_events': registered_events,
        'pinned_events': pinned_events,
    }
    return render(request, 'registered_events/registered.html', context)


def committee(request):
    if not check_profile_completed(request):
        # If profile is not complete, redirect to complete profile page
        return redirect('accounts:completeProfile', user_id=request.user.id)
    return render(request, 'committees/committee.html')


def about(request):
    return render(request, 'about/about.html')

def profile(request, username):
    user = get_object_or_404(User, username=username)
    profile = get_object_or_404(Profile, user=user)
    
    is_own_profile = False
    if request.user == user:
        is_own_profile = True


    context = {
        'user':user,
        'profile': profile,
        'is_own_profile': is_own_profile,

    }
    
    return render(request, 'StudentProfile/profile.html',context)

