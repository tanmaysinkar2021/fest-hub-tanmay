from django.contrib import messages
from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.decorators import login_required

from festhub.utils import check_profile_completed
from .forms import EventsForm
from .models import Event, UserEvent
from django.urls import reverse
from profiles.models import Profile

@login_required(login_url='accounts:login')
def events(request):
    if not check_profile_completed(request):
        # If profile is not complete, redirect to complete profile page
        return redirect('accounts:completeProfile', user_id=request.user.id)
    events = Event.objects.filter(published=True)
    pinned_events = UserEvent.objects.filter(user=request.user, pinned=True).values_list('event__uuid', flat=True)
    registered_events = UserEvent.objects.filter(user=request.user, registered=True).values_list('event__uuid', flat=True)
    user_committees = request.user.profile.committees.all()
    context={
        'events': events,
        'pinned_events': pinned_events,
        'registered_events': registered_events,
        'user_committees': user_committees,
    }
    return render(request, 'events/events.html',context)

@login_required(login_url='accounts:login')
def event(request, uuid):
    """Display details of a specific event."""
    if not check_profile_completed(request):
        # If profile is not complete, redirect to complete profile page
        return redirect('accounts:completeProfile', user_id=request.user.id)
    event_instance = get_object_or_404(Event, uuid=uuid)
    faqs = event_instance.faqs.all() 
    rules = event_instance.rules.all()
    reg=False
    user_interest = UserEvent.objects.get_or_create(user=request.user, event=event_instance)
    if user_interest[0].registered:
        reg=True
    
    context = {
        'event': event_instance,
        'faqs': faqs,  
        'rules': rules,
        'reg':reg,
    }
    return render(request, 'events/event.html', context)

@login_required(login_url='accounts:login')
def events_form(request):
    """Handle event creation form."""
    if not check_profile_completed(request):
        # If profile is not complete, redirect to complete profile page
        return redirect('accounts:completeProfile', user_id=request.user.id)
    
    if request.method == 'POST':
        form = EventsForm(request.POST, request.FILES)
        if form.is_valid():
            event = form.save()
            # Redirect to the event detail page
            return redirect('events:event', uuid=event.uuid)
    else:
        form = EventsForm()
    user_committees = request.user.profile.committees.all()
    context = {
        'form': form,
        'user_committees': user_committees,
    }
    return render(request, 'events/events_form.html',context)


@login_required(login_url='accounts:login')
def pin_event(request):
    if not check_profile_completed(request):
        # If profile is not complete, redirect to complete profile page
        return redirect('accounts:completeProfile', user_id=request.user.id)
    if request.method == 'POST':
        event_uuid = request.POST.get('event_uuid')
        event = get_object_or_404(Event, uuid=event_uuid)
        user_interest, created = UserEvent.objects.get_or_create(user=request.user, event=event)
        if not user_interest.pinned:
            user_interest.pinned = True
            user_interest.save()
            messages.success(request, 'Event pinned successfully!')
        else:
            messages.info(request, 'Event is already pinned!')
    return redirect('events:events')

@login_required(login_url='accounts:login')
def unpin_event(request):
    if not check_profile_completed(request):
        # If profile is not complete, redirect to complete profile page
        return redirect('accounts:completeProfile', user_id=request.user.id)
    if request.method == 'POST':
        event_uuid = request.POST.get('event_uuid')
        event = get_object_or_404(Event, uuid=event_uuid)
        interest = UserEvent.objects.filter(user=request.user, event=event, pinned=True).first()
        if interest:
            interest.pinned = False
            interest.save()
            messages.success(request, 'Event unpinned successfully!')
        else:
            messages.info(request, 'Event is not pinned!')
    return redirect('events:events')

@login_required(login_url='accounts:login')
def register_event(request):
    if not check_profile_completed(request):
        # If profile is not complete, redirect to complete profile page
        return redirect('accounts:completeProfile', user_id=request.user.id)

    if request.method == 'POST':
        event_uuid = request.POST.get('event_uuid')
        event = get_object_or_404(Event, uuid=event_uuid)
        user_interest, created = UserEvent.objects.get_or_create(user=request.user, event=event)
        if not user_interest.registered:
            user_interest.registered = True
            user_interest.save()
            messages.success(request, 'Event registered successfully!')
        else:
            messages.info(request, 'Event is already registered!')
    return redirect('events:events')


            
def registrations(request):
    if request.method == 'POST':
        event_uuid = request.POST.get('event_uuid')
        event = get_object_or_404(Event, uuid=event_uuid)
        registrations = UserEvent.objects.filter(event=event, registered=True)

        context = {
            'event': event,
            'registrations': registrations,
        }
        return render(request, 'events/registrations.html', context)

    return redirect('events:events')