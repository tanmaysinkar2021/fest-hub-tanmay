from django import forms
from.models import Event
from django.forms.widgets import SelectDateWidget
import uuid


class CustomTextInput(forms.TextInput):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('attrs', {})
        kwargs['attrs'].update({'class': 'formbold-form-input'})
        super().__init__(*args, **kwargs)

class CustomTextarea(forms.Textarea):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('attrs', {})
        kwargs['attrs'].update({'class': 'formbold-form-input'})
        super().__init__(*args, **kwargs)

class CustomSelect(forms.Select):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('attrs', {})
        kwargs['attrs'].update({'class': 'formbold-form-input'})
        super().__init__(*args, **kwargs)

class EventsForm(forms.ModelForm):
    banner = forms.ImageField(label='banner', required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            if isinstance(field.widget, (forms.TextInput, forms.Select, forms.Textarea)):
                field.widget.attrs.update({'class': 'formbold-form-input'})

    class Meta:
        model = Event
        fields = '__all__'
        
        widgets = {
            "date_time": forms.DateTimeInput(attrs={'placeholder': 'Enter date and time here...', 'type': 'datetime-local'}), 
            "name": forms.TextInput(attrs={'placeholder': 'Enter name here...'}),  
              
            "venue": forms.Textarea(attrs={'placeholder': 'Enter address here...', 'rows': 3}),  
            "prize_pool": forms.NumberInput(attrs={'placeholder': 'Enter prize pool here...', 'step': '100'}),  

            

            "nature": forms.Select(attrs={'placeholder': 'Select nature...'}),  
            "level": forms.Select(attrs={'placeholder': 'Select level...'}),  
            "event_type": forms.Select(attrs={'placeholder': 'Select event type...'}),  
            "contact_info": forms.TextInput(attrs={'placeholder': 'Enter contact info here...'}),  
            "email": forms.EmailInput(attrs={'placeholder': 'Enter email here...'}),  
            "website": forms.URLInput(attrs={'placeholder': 'Enter website here...'}),  
            "description": forms.Textarea(attrs={'placeholder': 'Enter description here...', 'rows': 8}),  
        }

        
        def save(self, commit=True):
            event = super().save(commit=False)
            event.uuid = uuid.uuid4()
            if commit:
                event.save()
            return event