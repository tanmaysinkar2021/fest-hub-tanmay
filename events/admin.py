from django.contrib import admin
from events.models import Event, Faq, Rule,UserEvent

class EventAdmin(admin.ModelAdmin):
    filter_horizontal = ('committees',) 

admin.site.register(Event, EventAdmin)
admin.site.register(Rule)
admin.site.register(UserEvent)
admin.site.register(Faq)
