from django import template

register = template.Library()

@register.filter
def intersect(queryset1, queryset2):
    return queryset1.intersection(queryset2)
